from django.shortcuts import render
from django.http import JsonResponse, HttpResponseRedirect
from django.urls import reverse
from google.oauth2 import id_token
from google.auth.transport import requests

def theDoor(request):
    return render(request, 'theDoor.html')


def thyProfile(request):
    if (request.method == "POST"):
        response = {}        
        request.session['userId'] = request.POST["id"]
        request.session['userName'] = request.POST["name"]
        request.session['userGivenName'] = request.POST["givenName"]
        request.session['userEmail'] = request.POST["emailUrl"]
        request.session['userAvatar'] = request.POST["avatar"]
        return HttpResponseRedirect(reverse('shelf'))

# idea from Stevany
def verify(request):
    if (request.method == "POST"):
        response = {
            'status' : "0"
        }
        try:
            toVerify = request.POST['token']
            info = id_token.verify_oauth2_token(toVerify, requests.Request(),"918159443378-2fgt9acknasvc7apdvgsj84rapr5mb5l.apps.googleusercontent.com")
            if (info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']):
                raise ValueError('Wrong issuer.')
        except ValueError:
            response["status"] = "1"
        return JsonResponse(response)

# idea from George
def imOut(request):
    request.session.flush()
    return HttpResponseRedirect(reverse("theDoor"))