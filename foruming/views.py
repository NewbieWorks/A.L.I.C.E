from django.shortcuts import render
from django.http import HttpResponse
from .models import Status
from .forms import StatusForm

hello = "Hello, Apa kabar?"

def index(request):
    html = "landing.html"
    fullSet = Status.objects.all()[Status.objects.count() - 9::] if Status.objects.count() > 9 else Status.objects.all()
    context = {
        'form' : StatusForm(),
        'fullSet' : fullSet,
        'hello' : hello
    }
    if (request.method == "POST"):
        form = StatusForm(request.POST)
        if (form.is_valid()):
            objective = Status(statText=request.POST['statText'])
            objective.save()
    return render(request, html, context)