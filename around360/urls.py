from django.urls import path
from .views import around

urlpatterns = [
    path('around360', around, name='around360'),
]