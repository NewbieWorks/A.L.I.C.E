from django.shortcuts import render
from .models import JadwalPribadi
from .forms import JadwalPribadiForm

def forming(request):
    html = 'forms.html'
    fullset = JadwalPribadi.objects.all()
    if (request.method == "POST"):
        if 'DeleteALL' in request.POST:
            JadwalPribadi.objects.all().delete()
            context = {
                    'form': JadwalPribadiForm(),
                    'fullset': fullset
            }
            return render(request, html, context)
        else:
            form = JadwalPribadiForm(request.POST, error_class=DivErrorList)
            if form.is_valid():
                response = {}
                message = form.save(commit=False)
                response['tanggal'] = request.POST['tanggal']
                response['day'] = getDay(response['tanggal'])
                response['jam'] = request.POST['jam']
                response['nama_kegiatan'] = request.POST['nama_kegiatan']
                response['tempat'] = request.POST['tempat']
                response['kategori'] = request.POST['kategori']
                message = JadwalPribadi(tanggal=response['tanggal'], nama_kegiatan=response['nama_kegiatan'],
                                        tempat=response['tempat'], kategori=response['kategori'],
                                        jam=response['jam'], day=response['day'])
                message.save()
                context = {
                    'form': JadwalPribadiForm(),
                    'fullset': fullset
                }
                return render(request, html, context)
    context = {
            'form': JadwalPribadiForm(),
            'fullset': fullset
        }
    return render(request, 'forms.html', context)


def getDay(dateRaw):
    datetime_object = datetime.strptime(dateRaw, '%Y-%m-%d')
    return calendar.day_name[datetime_object.date().weekday()]
