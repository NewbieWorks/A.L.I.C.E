from django.db import models
from django.utils import timezone

class JadwalPribadi(models.Model):
    day = models.TextField(editable=False, default="")
    tanggal = models.DateField(max_length=200)
    jam = models.TimeField(max_length=200)
    nama_kegiatan = models.CharField(max_length=200)
    tempat = models.CharField(max_length=200)
    kategori = models.CharField(max_length=200)