from django.urls import path
from .views import forming

urlpatterns = [
    path('forming', forming, name='forming'),
]