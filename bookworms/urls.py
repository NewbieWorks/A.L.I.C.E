from django.urls import path
from .views import shelf, book_json, newFav, noFav

urlpatterns = [
    path('shelf', shelf, name='shelf'),
    path('shelf/book/<variable>', book_json, name='book_json'),
    path('shelf/newFav', newFav, name="newFav"),
    path('shelf/noFav', noFav, name="noFav"),
]