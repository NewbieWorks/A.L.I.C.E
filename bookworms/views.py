from django.shortcuts import render
from django.http import JsonResponse, HttpResponseRedirect
from django.urls import reverse
import urllib.request
import json
import requests


def shelf(request):
    response = {}
    if ('userId' not in request.session.keys()):
        return HttpResponseRedirect(reverse("theDoor"))
    if ('shelf' in request.session.keys()):
        theShelf = request.session['shelf']
    else:
        theShelf = []
    response['name'] = request.session['userGivenName']
    response['totalFav'] = len(theShelf)
    return render(request, "shelf.html", response)


def book_json(request, variable):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + variable
    # gottenResponse = urllib.request.urlopen(url)
    # data = json.loads(gottenResponse.read())
    jsonFromUrl = requests.get(url).json()
    items = jsonFromUrl['items']
    fullLib = []
    if ('shelf' not in request.session.keys()):
        request.session['shelf'] = []

    for item in items:
        bookId = item["id"]
        focus = item['volumeInfo']
        favEnable = bookId in request.session['shelf']
        bookSet = {
            'bookId' : bookId,
            'focus' : focus,
            'fav' : favEnable
        }
        fullLib.append(bookSet)
    
    response = {
        'fullLib' : fullLib
    }
    return JsonResponse(response)


def newFav(request):
    response = {}
    if ('userId' not in request.session.keys()):
        return HttpResponseRedirect(reverse("theDoor"))
    if (request.method == "POST"):
        bookId = request.POST['bookId']
        response['bookId'] = bookId
        if ('shelf' in request.session.keys()):
            theShelf = request.session['shelf']
            theShelf.append(bookId)
            request.session['shelf'] = theShelf
            response['totalFav'] = len(theShelf)
        else:
            request.session['shelf'] = [bookId]
            response['totalFav'] = 1
        return JsonResponse(response)

def noFav(request):
    response = {}
    if ('userId' not in request.session.keys()):
        return HttpResponseRedirect(reverse("theDoor"))
    if (request.method == "POST"):
        bookId = request.POST['bookId']
        response['bookId'] = bookId
        theShelf = request.session['shelf']
        theShelf.remove(bookId)
        request.session['shelf'] = theShelf
        response['totalFav'] = len(theShelf)
        return JsonResponse(response)
