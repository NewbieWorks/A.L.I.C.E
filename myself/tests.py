from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from django.contrib.auth import get_user_model
from django.contrib.staticfiles.storage import staticfiles_storage 
from django.contrib.staticfiles import finders
from django_dynamic_fixture import G
import unittest
from .views import myName, desc, profile

class ProfileTest(TestCase):
    def test_link_available(self):
        response = Client().get('/profile')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'profile.html')

    def test_content_hello(self):
        request = HttpRequest()
        response = profile(request) 
        htmlResponse = response.content.decode('utf8')
        self.assertIn('Welcome to My Profile', htmlResponse)
    
    def test_check_avatar(self):
        request = HttpRequest()
        response = profile(request) 
        htmlResponse = response.content.decode('utf8')
        self.assertIn('id="avatar"', htmlResponse)

    def test_check_myName(self):
        request = HttpRequest()
        response = profile(request) 
        htmlResponse = response.content.decode('utf8')
        self.assertIn(myName, htmlResponse)
    
    def test_check_myDesc(self):
        request = HttpRequest()
        response = profile(request) 
        htmlResponse = response.content.decode('utf8')
        self.assertIn(desc, htmlResponse)
    
    def test_check_fullPart(self):
        request = HttpRequest()
        response = profile(request) 
        htmlResponse = response.content.decode('utf8')
        self.assertIn("#core", htmlResponse)
        self.assertIn("#profile", htmlResponse)
        self.assertIn("#edu", htmlResponse)
        self.assertIn("#achievementContainer", htmlResponse)
        self.assertIn("#skill", htmlResponse)