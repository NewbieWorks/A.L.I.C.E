from django.urls import path
from .views import accorDio
from django.views.generic import RedirectView

urlpatterns = [
    path('accorDio', accorDio, name='accorDio'),
]