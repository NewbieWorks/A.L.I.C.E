from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
import unittest
import time
from .views import accorDio
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.firefox.options import Options


class FaqUnitTest(TestCase):

    def test_Faq_is_exist_using_templates(self):
        response = Client().get('/accorDio')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'accordio.html')

    def test_content_Faq(self):
        request = HttpRequest()
        response = accorDio(request)
        htmlResponse = response.content.decode('utf8')
        self.assertIn('Frequently', htmlResponse)
        self.assertIn('Asked', htmlResponse)
        self.assertIn('Question', htmlResponse)

class CobaCobaTest(TestCase):
    def setUp(self):
        capabilities = {
            'browserName': 'chrome',
            'chromeOptions':  {
                'useAutomationExtension': False,
                'forceDevToolsScreenshot': True,
                'args': ['--headless', '--no-sandbox', '--disable-dev-shm-usage']
            }
        }
        self.browser = webdriver.Chrome(
            './chromedriver', desired_capabilities=capabilities)

    def tearDown(self):
        self.browser.quit()

    def test_check_title_exist(self):
        browser = self.browser
        browser.get('http://alicization-begin.herokuapp.com/accorDio')
        time.sleep(5)
        self.assertEqual("\'s FAQ", browser.title)
        self.tearDown()

    def test_check_Faq_text(self):
        browser = self.browser
        browser.get('http://alicization-begin.herokuapp.com/accorDio')
        time.sleep(5)
        faqText = browser.find_element_by_id("freq").text
        self.assertIn('Frequently', faqText)
        self.tearDown()

    def test_faq_text_has_desired_class(self):
        browser = self.browser
        browser.get('http://alicization-begin.herokuapp.com/accorDio')
        time.sleep(5)
        welcomeText = browser.find_element_by_id("line1")
        self.assertIn("fullHeight", welcomeText.get_attribute("class"))
        self.assertIn("titleText", welcomeText.get_attribute("class"))
        self.tearDown()

    def test_welcome_bg_has_desired_class(self):
        browser = self.browser
        browser.get('http://alicization-begin.herokuapp.com/accorDio')
        time.sleep(5)
        headBg = browser.find_element_by_id("head")
        self.assertIn("fullHeight", headBg.get_attribute("class"))
        self.assertIn("header", headBg.get_attribute("class"))
        self.tearDown()

    def test_light_button_clickable(self):
        browser = self.browser
        browser.get('http://alicization-begin.herokuapp.com/accorDio')
        time.sleep(5)
        light = browser.find_element_by_id("light")
        light.click()
        self.tearDown()

    def test_dark_button_clickable(self):
        browser = self.browser
        browser.get('http://alicization-begin.herokuapp.com/accorDio')
        time.sleep(5)
        dark = browser.find_element_by_id("dark")
        dark.click()
        self.tearDown()

    def test_toggle_button_clickable(self):
        browser = self.browser
        browser.get('http://alicization-begin.herokuapp.com/accorDio')
        time.sleep(5)
        toggle = browser.find_element_by_id("toggle")
        toggle.click()
        self.tearDown()
    
    def test_accordion_button_clickable(self):
        browser = self.browser
        browser.get('http://alicization-begin.herokuapp.com/accorDio')
        time.sleep(5)
        faq1 = browser.find_element_by_id("faq1")
        faq1.click()
        time.sleep(5)
        fullName = browser.find_element_by_id("fullName").text
        self.assertIn("Nathanael", fullName)
        self.tearDown()