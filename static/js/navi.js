var verticalLine = document.getElementById("verticalLine");
var content = document.getElementById("content");
var right = document.getElementById("rightMod");
var active = false;

function hideAll() {
    right.style.display = "none";
    verticalLine.style.display = "none";
    content.style.display = "none";
}

$(document).ready(function() {
    hideAll();
    var $hamburger = $(".hamburger");
    $hamburger.on("click", function(e) {
        $hamburger.toggleClass("is-active");
    });
});

function showContent() {
    if (content.style.display == "block") {
        hideAll();
    } else {
        hideAll();
        right.style.display = "block";
        verticalLine.style.display = "block";
        content.style.display = "block";
    }
}