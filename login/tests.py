from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from django_dynamic_fixture import G
import unittest
import time
from .views import login, checkEmail, create, viewAll, removal
from .models import Subscriber
from .forms import SubscriberForm
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.firefox.options import Options


class SubsribeTest(TestCase):

    def test_subscribe_is_exist_using_templates(self):
        response = Client().get('/subscribe')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')

    def test_model_available(self):
        G(Subscriber)
        G(Subscriber)

        modelInDb = Subscriber.objects.all()
        self.assertEquals(modelInDb.count(), 2)

    def test_form_available(self):
        form = SubscriberForm()
        self.assertIn("username", form.as_p())
        self.assertIn("email", form.as_p())
        self.assertIn("password", form.as_p())

    def test_form_empty_input(self):
        data = {'username': '', 'email' :'', 'password':''}
        form = SubscriberForm(data)
        self.assertFalse(form.is_valid())

    def test_form_to_model(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST['username'] = "nathanael"
        request.POST['email'] = "nathanael@leanahtan.com"
        request.POST['password'] = "leanahtan"
        response = create(request)
        self.assertEqual(response.status_code, 200)

    def test_multi_email(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST['username'] = "nathanael"
        request.POST['email'] = "nathanael2@leanahtan.com"
        request.POST['password'] = "leanahtan"
        create(request)
        self.assertTrue(Subscriber.objects.filter(email=request.POST['email']).exists())

    def test_check_mail_function(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST['email'] = 'evo220999@gmail.com'
        response = checkEmail(request)
        self.assertEqual(response.status_code, 200)

    def test_check_get_full_function(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST['asking'] = True
        response = viewAll(request)
        self.assertEqual(response.status_code, 200)

    def test_viewers_is_exist_using_templates(self):
        response = Client().get('/subscribe/viewers')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'thanks.html')

    def test_can_delete(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST['toRemove'] = 'evo220999@gmail.com'
        response = removal(request)
        self.assertEqual(response.status_code, 200)

class CobaCobaTest(TestCase):
    def setUp(self):
        capabilities = {
            'browserName': 'chrome',
            'chromeOptions':  {
                'useAutomationExtension': False,
                'forceDevToolsScreenshot': True,
                'args': ['--headless', '--no-sandbox', '--disable-dev-shm-usage']
            }
        }
        self.browser = webdriver.Chrome(
            './chromedriver', desired_capabilities=capabilities)

    def tearDown(self):
        self.browser.quit()

    def test_insert_coba_coba(self):
        browser = self.browser
        browser.get('http://alicization-begin.herokuapp.com/subscribe')
        time.sleep(5)
        inputFormUname = browser.find_element_by_id("id_username")
        inputFormEmail = browser.find_element_by_id("id_email")
        inputFormPassword = browser.find_element_by_id("id_password")
        inputFormUname.send_keys("Coba Coba")
        inputFormEmail.send_keys("leviathan@gmail.xom")
        inputFormPassword.send_keys("hahahahahaha")
        time.sleep(5)
        browser.find_element_by_id("submit").click()
        self.tearDown()

    def test_check_title_exist(self):
        browser = self.browser
        browser.get('http://alicization-begin.herokuapp.com/subscribe')
        time.sleep(5)
        self.assertEqual("\'s Subscribe Form", browser.title)
        self.tearDown()

    def test_check_welcome_text(self):
        browser = self.browser
        browser.get('http://alicization-begin.herokuapp.com/subscribe')
        time.sleep(5)
        topText = browser.find_element_by_id("topText").text
        self.assertIn('Join as Subscriber', topText)
        self.tearDown()